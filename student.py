from flask import *
import sqlite3
import requests
import json


app = Flask(__name__)

@app.route('/student/<_id>', methods = ['GET'])
def get_by_id(_id):
	conn = sqlite3.connect('stud.db')
	cur = conn.cursor()
	if(_id!= None):
		cur.execute('SELECT * FROM STUDENT where id=?',[_id])
		row = cur.fetchone()
		print(row[0],row[1],row[2],row[3])
		return jsonify(row)

@app.route('/student',methods = ['GET'])
def getall(self):
	conn = sqlite3.connect('stud.db')
	cur = conn.cursor()
	cur.execute('SELECT * FROM STUDENT')
	data = cur.fetchall()
	for stud in data:
		print(stud[0],stud[1],stud[2],stud[3])
	return data

@app.route('/student' ,methods = ['POST'])
def poststud():
	conn = sqlite3.connect('stud.db')
	cur = conn.cursor()
	_id = request.args.get('_id')
	firstname = request.args.get('firstname')
	lastname = request.args.get('lastname')
	pnum = request.args.get('pnum')
	cur.execute('INSERT INTO STUDENT(id,firstname,lastname,pnum) VALUES(?,?,?,?)',(_id,firstname,lastname,pnum))
	return "Exotel"
	return jsonify(request.get_json())

if __name__ == '__main__':
	app.run(debug=False)